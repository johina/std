package com.example.studentboxs;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

class StudentMainFile extends AppCompatActivity {
    RecyclerView studentList_rv;
    StudentAdapter studentAdapter;
    List<com.example.studentboxs.StudentMain> StudentMain  = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_main);

        StudentMain.add(new StudentMain("johina mohammed ","20151281","86%","5"));//
        StudentMain.add(new StudentMain("farook mohammed ","20163991","85%","4"));//
        StudentMain.add(new StudentMain("heba mohammed ","20178531","72%","3"));//
        StudentMain.add(new StudentMain("alaa mohammed ","20188231","75%","2"));//
        StudentMain.add(new StudentMain("noor mohammed ","20163331","92%","4"));//
        StudentMain.add(new StudentMain("haya mohammed ","20159871","87%","5"));//


        studentList_rv = findViewById(R.id.studentList_rv);
        studentList_rv.setLayoutManager(new LinearLayoutManager(this));
        studentAdapter = new StudentAdapter(this ,StudentMain);
        studentList_rv.setAdapter(studentAdapter);



    }
}