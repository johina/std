package com.example.studentboxs;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.studentboxs.R;
import com.example.studentboxs.StudentMain;

import java.util.List;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.Student> {

    Context context ;
    List<StudentMain> StudentMain;


    public StudentAdapter(Context context ,  List<StudentMain> StudentMain) {
        this.context = context;
        this.StudentMain = StudentMain;
    }

    @NonNull
    @Override
    public Student onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =LayoutInflater.from(context).inflate(R.layout.activity_student_main , parent , false);
        return new Student(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Student holder, int position) {

        holder.setData(StudentMain.get(position));
    }

    @Override
    public int getItemCount() {
        return StudentMain.size();
    }

    class Student extends RecyclerView.ViewHolder{
        TextView  name_std , id_std , avg_std , level_std  , Student_Id;

        public Student(@NonNull View itemView) {
            super(itemView);
            name_std = itemView.findViewById(R.id.name_std);
            id_std = itemView.findViewById(R.id.id_std);
            avg_std = itemView.findViewById(R.id.avg_std);
            level_std = itemView.findViewById(R.id.level_std);
        }

        public void setData(final StudentMain std) {
            name_std.setText(std.getName());
            id_std.setText(std.getIdStd());
            avg_std.setText(std.getAvarege());
            level_std.setText(std.getLevel());


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context,std.getName(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(itemView.getContext() ,MainActivity.class);
                    intent.putExtra("id",std.getIdStd());
                    intent.putExtra("name",std.getName());
                    intent.putExtra("avarage",std.getAvarege());
                    intent.putExtra("level",std.getLevel());
                    itemView.getContext().startActivity(intent);
                }
            });

        }
    }
}
